with params as (
	select
        '{country}' as country,
        '{platform_group}' as platform_group,
        '{new_user_stt_date}'::date as new_user_stt_date,
        '{new_user_end_date}'::date as new_user_end_date
		
), app_install_traffic_source as (
-- Appsflyer Install + Appsflyer Converstion Retargeting Event Tables
-- ONE Touch ONE Row
	select
		NVL(idfv, idfa, advertising_id) as device_id,
		appsflyer_id,
		event_time + interval '1 hour' * 8 as event_time_hk,
		date_trunc('day', event_time_hk) as event_date_hk,
		date_trunc('month', event_time_hk) as event_month_hk,
		attributed_touch_type,
		event_name,
		retargeting_conversion_type,
		is_retargeting,
		attributed_touch_time,
		media_source as af_media_source,
		campaign as af_campaign,
		af_c_id as af_c_id,
		af_adset,
		af_channel,
		row_number() over (partition by device_id, event_month_hk, event_name order by event_time_hk) as rn_month
	from s3.af_dl_installs adi
	where
		data_date between date_trunc('month', (select new_user_stt_date from params)) and date_trunc('month', (select new_user_end_date from params))
		
), app_uninstall as (
	select
		appsflyer_id,
		min(event_time + interval '1 hour' * 8) as event_time_hk
	from s3.af_dl_uninstalls au 
	where data_date >= (select new_user_stt_date from params)
	group by 1

), parsed_ddvv as (
	select
		floor(date_diff('day', d.device_first_visit_date, current_date) / 30) as device_current_m_period,
		floor(date_diff('day', d.device_first_visit_date, d.visit_date_hk) / 30) as m_period,
		(d.content_window_indicator and d.video_dur_min >= 1) or (not d.content_window_indicator and d.video_dur_min >= 2) as is_analytical_vv,
		country,
		device_id,
		app_ses_cde,
		platform_group,
		platform_name,
		visit_stt_tms_hk,
		visit_date_hk,
		vv_date_hk,
		traffic_source,
		traffic_domain,
		utm_src,
		utm_medium,
		user_id,
		user_mode,
		user_create_date,
		video_stt_tms_hk,
		d.product_cat_name,
		product_grp_sri_nme,
		product_sri_cms_id,
		content_window_indicator,
		vv_indr,
		video_dur_min,
		device_first_visit_date
	from bi.ddvv_004 d
	where
		-- useful rows --
		d.visit_date_hk between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params) and -- m0 only
		d.country = (select country from params) and
		platform_group = (select platform_group from params) and
		-- selected cohort only --
		d.device_first_visit_date between (select new_user_stt_date from params) and (select new_user_end_date from params) and
		-- M0 Normal Behavior only--
		nvl(d.visit_type, 'Normal') = 'Normal' and
		nvl(d.product_sri_nme, '') <> 'Unknown' -- Include non vv rows and vv rows without unknown title
    
), m0_new_device_record as (
	select 
		d.country,
		d.device_id,
		case when min(d.platform_name) like 'Android%' then '__g'
				 when min(d.platform_name) like 'IOS%' then '-v' end + lower(replace(d.device_id, '-', '')) as clevertapid_diy,
		min(d.device_first_visit_date) as device_first_visit_date,
		min(d.platform_group) as platform_group,
		min(d.platform_name) as platform_name,
		min(d.device_current_m_period) as device_current_m_period,
		min(d.visit_stt_tms_hk) as first_visit_tms_hk,
		
		min(d.visit_stt_tms_hk::varchar + '_' + d.traffic_source + '_' + nvl(d.utm_src, '') + '_' + nvl(d.utm_medium, '') + '_' + nvl(d.traffic_domain)) as acquisition_utm,
		split_part(acquisition_utm, '_', 2) as acquisition_traffic_source,
		split_part(acquisition_utm, '_', 5) as acquisition_traffic_domain,
		split_part(acquisition_utm, '_', 3) as acquisition_utm_source,
		split_part(acquisition_utm, '_', 4) as acquisition_utm_medium,
		
		max(case when d.user_id <> - 1 then d.user_id end) as user_id,
		max(d.user_create_date)::date as user_create_date,
		max(d.user_mode) as user_mode,
		max(d.visit_stt_tms_hk) as last_m0_visit_tms_hk,
		max(d.video_stt_tms_hk) as last_m0_vv_tms_hk,
		count(distinct d.visit_date_hk) as m0_total_visit_days,
		count(distinct d.app_ses_cde) as m0_total_sessions,
		nvl(count(distinct d.vv_date_hk)) as m0_total_vv_days,
		nvl(sum(d.vv_indr), 0) as m0_total_vv,
		nvl(sum(d.video_dur_min), 0) as m0_total_video_dur_min,
		nvl(count(distinct case when d.is_analytical_vv then d.product_grp_sri_nme end), 0) as m0_title_watched,

		min(case when d.is_analytical_vv then
                d.video_stt_tms_hk::varchar + '_' +
                d.content_window_indicator::int::varchar + '_' +
                d.product_sri_cms_id::varchar + '_' +
                d.product_grp_sri_nme + '_' +
                d.product_cat_name end) as acquisition_analytical_title,
		split_part(acquisition_analytical_title, '_', 1)::timestamp as acqusition_analytical_vv_tms_hk,
		split_part(acquisition_analytical_title, '_', 2)::int::boolean as acqusition_analytical_is_preview,
		split_part(acquisition_analytical_title, '_', 3)::int as acqusition_analytical_sri_cms_id,
		split_part(acquisition_analytical_title, '_', 4) as acqusition_analytical_grp_sri_nme,
		split_part(acquisition_analytical_title, '_', 5) as acqusition_analytical_cat_name,

		min(
            d.video_stt_tms_hk::varchar + '_' +
            d.content_window_indicator::int::varchar + '_' +
            d.product_sri_cms_id::varchar + '_' +
            d.product_grp_sri_nme + '_' +
            d.product_cat_name
        ) as acquisition_title,
		split_part(acquisition_title, '_', 1)::timestamp as acqusition_vv_tms_hk,
		split_part(acquisition_title, '_', 2)::int::boolean as acqusition_is_preview,
		split_part(acquisition_title, '_', 3)::int as acqusition_sri_cms_id,
		split_part(acquisition_title, '_', 4) as acqusition_grp_sri_nme,
		split_part(acquisition_title, '_', 5) as acqusition_cat_name
	from parsed_ddvv d
	group by 1,2
	
), m0_search as (
	select
		d.country,
	  d.device_id,
	  max(case when search_keyword_user_input is not null then
					  			(event_timestamp + interval '1 hour' * 8)::varchar + '|' +
					  			nvl(replace(replace(search_keyword,'|',' '),',',' '), '') + '|' +
					  			replace(replace(search_keyword_user_input,'|',' '),',',' ') + '|' +
					  			nvl(search_keyword_group, '') + '|' + 
					  			nvl(search_results_found, '0') end)
					  			as last_search_info,
		split_part(last_search_info, '|', 1)::timestamp as last_search_tms_hk,
		split_part(last_search_info, '|', 2) as last_search_kw,
		split_part(last_search_info, '|', 3) as last_search_kw_input,
		nullif(split_part(last_search_info, '|', 4),'') as last_search_kw_group,
		split_part(last_search_info, '|', 5) as last_search_result_found,
		count(*) as total_search
	from datalake.raw_data d
	join bi.d_dvc_vst dd
		on dd.device_id = d.device_id
		and dd.country = d.country
		and (event_timestamp + interval '1 hour' * 8)::date between dd.device_first_visit_date and dd.device_first_visit_date + interval '1 day' * 29
	where 
	    -- Useful Rows Only --
	    d.arrival_date between (select new_user_stt_date - interval '1 day' from params) and (select new_user_end_date + interval '1 day' * 29 from params) and
	    -- M0 Only --
	    date_diff('day', dd.device_first_visit_date, (d.event_timestamp + interval '1 hour' * 8)::date ) between 0 and 29 and
	    -- Selected Cohort Only --
	    dd.device_first_visit_date between (select new_user_stt_date from params) and (select new_user_end_date from params) and
			d.country = (select country from params) and
			d.event_action = 'Search Result Loaded'
	group by 1, 2
	
), m0_cat_screen as (
	select
		device_id,
		country,
		max(case when rn_tms = 1 then cat_name end) as m0_last_click_cat_screen,
		max(case when rn_total = 1 then cat_name end) as m0_most_click_cat_screen,
		sum(total_screenview) as m0_total_screenview
	from (
		select
			*,
			row_number() over (partition by device_id, country order by last_screenview_tms_hk desc) as rn_tms,
			row_number() over (partition by device_id, country order by total_screenview desc) as rn_total
		from (
			select
				d.device_id,
				d.country,
				nvl(gcn.grp_cat_name, event_label) as cat_name,
				max(event_timestamp + interval '1 hour' * 8) as last_screenview_tms_hk,
				count(*) as total_screenview
			from datalake.raw_data d
			join bi.d_dvc_vst dd
				on dd.device_id = d.device_id
				and dd.country = d.country
				and (event_timestamp + interval '1 hour' * 8)::date between dd.device_first_visit_date and dd.device_first_visit_date + interval '1 day' * 29
			left join mi_dev.group_category_name gcn on gcn.cat_name = d.event_label
			where 
			    -- Useful Rows Only --
			    d.arrival_date between (select new_user_stt_date - interval '1 day' from params) and (select new_user_end_date + interval '1 day' * 29 from params) and
			    -- M0 Only --
			    date_diff('day', dd.device_first_visit_date, (d.event_timestamp + interval '1 hour' * 8)::date ) between 0 and 29 and
			    -- Selected Cohort Only --
			    dd.device_first_visit_date between (select new_user_stt_date from params) and (select new_user_end_date from params) and
					d.country = (select country from params) and
					d.event_screen = 'Category' and
					d.event_action = 'click or swipe' and
					d.event_label <> '(null)'
			group by 1,2,3
		)
	)
	group by 1,2
	
), m0_ct_app_launch_ctid as ( -- EVENT from CT SDK
		select
			profile_objectid as clevertapid,
			max(profile_push_token) as ct_m_profile_push_token,
			min(to_timestamp(ts, 'YYYYMMDDHH24MISS')::timestamp) as ct_m_first_launch_tms_hk,
      count(*) as total_app_launch
		from
			(
				select
					ts_to_date,
					profile_objectid,
					profile_push_token,
					ts,
					min(ts_to_date) over (partition by profile_objectid) as first_date
				from s3.clevertap_app_launch cal
				where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
			)
		where datediff('day', first_date, ts_to_date) < 30
		group by 1

), m0_ct_app_launch_uid as ( -- EVENT from CT SDK
		select
				REGEXP_SUBSTR(profile_identity , '(\\d+)$', 1) as user_id,
				max(profile_push_token) as u_m_profile_push_token,
				min(to_timestamp(ts, 'YYYYMMDDHH24MISS')::timestamp) as u_m_first_launch_tms_hk,
        count(*) as total_app_launch
		from
			(
				select
					ts_to_date,
					profile_identity,
					profile_push_token,
					ts,
					min(ts_to_date) over (partition by profile_identity) as first_date
				from s3.clevertap_app_launch cal
				where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
			)
		where datediff('day', first_date, ts_to_date) < 30
		group by 1
		
), m0_ct_vv_ctid as ( -- EVENT from CT SDK
	select
			profile_objectid as clevertapid,
			max(profile_push_token) as ct_m_profile_push_token,
			min(to_timestamp(ts, 'YYYYMMDDHH24MISS')::timestamp) as ct_m_first_vv_tms_hk
	from
			(
				select
					ts_to_date,
					profile_objectid,
					profile_push_token,
					ts,
					min(ts_to_date) over (partition by profile_objectid) as first_date
				from s3.clevertap_event_video_watching cevw
				where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
			)
	where datediff('day', first_date, ts_to_date) < 30
	group by 1
	
), m0_ct_vv_uid as ( -- EVENT from CT SDK
	select
			REGEXP_SUBSTR(profile_identity , '(\\d+)$', 1) as user_id,
			max(profile_push_token) as u_m_profile_push_token,
			min(to_timestamp(ts, 'YYYYMMDDHH24MISS')::timestamp) as u_m_first_vv_tms_hk
	from
			(
				select
					ts_to_date,
					profile_identity,
					profile_push_token,
					ts,
					min(ts_to_date) over (partition by profile_identity) as first_date
				from s3.clevertap_event_video_watching cevw
				where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
			)
	where datediff('day', first_date, ts_to_date) < 30
	group by 1
	
), m0_ct_control_group_n_noti_sent_ctid as (
	select
		profile_objectid as clevertapid,
		sum(case when eventname in ('Control Group', 'Journey Control Group') then 1 else 0 end) as system_control_group_count,
		min(case when eventname in ('Control Group', 'Journey Control Group') then ts end) as first_system_control_group_tms_hk,
		sum(case when eventname = 'Notification Sent' then 1 else 0 end) as noti_sent_count,
		min(case when eventname = 'Notification Sent' then ts end) as first_noti_sent_tms_hk,
		sum(case when eventname = 'Notification Clicked' then 1 else 0 end) as noti_click_count,
		min(case when eventname = 'Notification Clicked' then ts end) as first_noti_click_tms_hk
	from (
		select
			*, min(ts_to_date) over (partition by profile_objectid) as first_date
		from (
			select profile_objectid, ts_to_date, eventname, TO_TIMESTAMP(ts, 'YYYYMMDDHH24MISS')::timestamp as ts from s3.clevertap_system_control_group cscg 
			where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
			
			union all
			
			select profile_objectid, ts_to_date, eventname, TO_TIMESTAMP(ts, 'YYYYMMDDHH24MISS')::timestamp as ts from s3.clevertap_journey_system_control_group cjscg 
			where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)		
			
			union all
			
			select profile_objectid, ts_to_date, eventname, TO_TIMESTAMP(ts, 'YYYYMMDDHH24MISS')::timestamp as ts from s3.clevertap_notification_sent cns
			where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
			
			union all
			select profile_objectid, ts_to_date, eventname, TO_TIMESTAMP(ts, 'YYYYMMDDHH24MISS')::timestamp as ts from s3.clevertap_notification_clicked cnc 
			where ts_to_date between (select new_user_stt_date from params) and (select new_user_end_date + interval '1 day' * 29 from params)
		)
		where profile_objectid <> ''
	) main
	where datediff('day', first_date, ts_to_date) < 30
	group by 1
	-- Excluding the profile which are in system_control_group but still got notification sent
	-- Avoid the cases when marketers remove the system control group during config of campaign
	having not (system_control_group_count > 0 and noti_sent_count > 0)
	
), all_titles as (
	select distinct vpm.series_name from bi.vw_pd_mst vpm 
    
), m0_ad_views as (
    
    select
        rd.country,
        rd.device_id,
        ddv.device_first_visit_date,
        sum(case when rd.event_category = 'VAST Ad' and rd.event_action = 'Started' then 1 else 0 end) as m0_total_ad_start,
        sum(case when rd.event_category = 'VAST Ad' and rd.event_action = 'Completed' then 1 else 0 end) as m0_total_ad_complete
    from datalake.raw_data rd
    join bi.d_dvc_vst ddv on ddv.device_id = rd.device_id
    where
        -- Useful Rows Only --
        rd.arrival_date between (select new_user_stt_date - interval '1 day' from params) and (select new_user_end_date + interval '1 day' * 29 from params) and
        -- M0 Only --
        date_diff('day', ddv.device_first_visit_date, (rd.event_timestamp + interval '1 hour' * 8)::date ) between 0 and 29 and
        -- Selected Cohort Only --
        ddv.device_first_visit_date between (select new_user_stt_date from params) and (select new_user_end_date from params) and
        -- Select Specific Events Only --
        rd.event_category in ('VAST Ad')
    group by rd.country, rd.device_id, ddv.device_first_visit_date

)

select
	current_date as data_extraction_date,
	-- Meta Info --
	ndr.country,
	ndr.device_id,
	calc.clevertapid,
	aits.appsflyer_id,
	ndr.device_first_visit_date,
	ndr.first_visit_tms_hk,
	ndr.platform_group,
	ndr.platform_name,
	ndr.device_current_m_period,
	
	-- UTM / AF Acqusition Source --
	ndr.acquisition_traffic_source,
	ndr.acquisition_traffic_domain,
	ndr.acquisition_utm_source,
	ndr.acquisition_utm_medium,
	aits.af_media_source as acquisition_af_media_source,
	aits.event_time_hk as acquisition_af_install_tms_hk,
	au.event_time_hk as acquisition_af_lt_uninstall_tms_hk,
	case when aits.event_time_hk is not null then nvl(ougm_af.channel_grouping, 'org_other')
			 when ndr.acquisition_utm_source <> '' then nvl(ougm.channel_grouping, 'org_other')
			 when rmdm.channel_grouping is not null then rmdm.channel_grouping
			 else 'org_direct' end as acqusition_channel_group,
			 
	-- M0 Login --
	ndr.user_id as m0_user_id,
	ndr.user_create_date as m0_user_create_date,
	ndr.user_mode as m0_user_mode,
	dur.nck_nme as m0_user_nick_name,
	dur.usr_nme as m0_user_name,
	dur.sol_acc_eml as m0_user_sol_acc_eml,
	sam.account_type as m0_user_account_type,
	case when nvl(m0_user_nick_name, '') like '%@%' then m0_user_nick_name
       when nvl(m0_user_sol_acc_eml, '') like '%@%' then m0_user_sol_acc_eml end as m0_user_email,
	m0_user_email is not null as m0_user_with_email,
	
	-- M0 Recency --
	ndr.last_m0_visit_tms_hk as m0_last_visit_tms_hk,
	ndr.last_m0_vv_tms_hk as m0_last_vv_tms_hk,
	
 	-- M0 Engagement --
	ndr.m0_total_visit_days,
	ndr.m0_total_sessions,
	ndr.m0_total_vv_days,
	ndr.m0_total_vv,
	ndr.m0_total_video_dur_min,
	ndr.m0_title_watched,
	
	ndr.acqusition_vv_tms_hk,
	ndr.acqusition_is_preview,
	ndr.acqusition_sri_cms_id,
	ndr.acqusition_grp_sri_nme,
	nvl(gcn.grp_cat_name, ndr.acqusition_cat_name) as acqusition_grp_cat_name,
	
	ndr.acqusition_analytical_vv_tms_hk,
	ndr.acqusition_analytical_is_preview,
	ndr.acqusition_analytical_sri_cms_id,
	ndr.acqusition_analytical_grp_sri_nme,
	nvl(gcn2.grp_cat_name, ndr.acqusition_analytical_cat_name) as acqusition_analytical_grp_cat_name,
    
    -- M0 Ad Views --
    av.m0_total_ad_start,
	
	-- Search --
	ms.last_search_tms_hk as m0_last_search_tms_hk,
	ms.last_search_kw as m0_last_search_kw,
	att.series_name is not null as m0_last_search_kw_exact_match,
	ms.last_search_kw_input as m0_last_search_kw_input,
	ms.last_search_kw_group as m0_last_search_kw_group,
	ms.last_search_result_found as m0_last_search_result_found,
	nvl(ms.total_search, 0) as m0_total_search,
	
	-- Category Screen --
	mcs.m0_last_click_cat_screen,
	mcs.m0_most_click_cat_screen,
	mcs.m0_total_screenview,
	
	-- CT App Launch --
  case when calc.ct_m_first_launch_tms_hk is not null then calc.ct_m_profile_push_token
			 when calu.u_m_first_launch_tms_hk is not null then calu.u_m_profile_push_token end as m0_ct_app_launch_push_token,
	nvl(calc.ct_m_first_launch_tms_hk, calu.u_m_first_launch_tms_hk) as m0_ct_first_app_launch_tms_hk,
  case when calc.ct_m_first_launch_tms_hk is not null then calc.total_app_launch
			 when calu.u_m_first_launch_tms_hk is not null then calu.total_app_launch end as m0_ct_total_app_launch,
	case when calc.ct_m_first_launch_tms_hk is not null then 'from_clevertap_id'
			 when calu.u_m_first_launch_tms_hk is not null then 'from_user_id' end as m0_ct_app_launch_map_source,
			 
	-- CT Video View --
	nvl(cvc.ct_m_profile_push_token, cvu.u_m_profile_push_token) as m0_ct_vv_push_token,
	nvl(cvc.ct_m_first_vv_tms_hk, cvu.u_m_first_vv_tms_hk) as m0_ct_vv_first_vv_tms_hk,
	case when cvc.ct_m_first_vv_tms_hk is not null then 'from_clevertap_id'
			 when cvu.u_m_first_vv_tms_hk is not null then 'from_user_id' end as m0_ct_vv_map_source,
			 
	-- CT Control Group --
	ccg.system_control_group_count as m0_system_control_group_count,
	ccg.noti_sent_count as m0_noti_sent_count,
	ccg.noti_click_count as m0_noti_click_count
from m0_new_device_record ndr
left join app_install_traffic_source aits on ndr.platform_group = 'APP'
                                            and aits.device_id = ndr.device_id
                                            and aits.event_month_hk = date_trunc('month', ndr.device_first_visit_date)
                                            and aits.rn_month = 1
left join app_uninstall au on au.appsflyer_id = aits.appsflyer_id
left join m0_search ms on ms.country = ndr.country and ms.device_id = ndr.device_id
left join m0_ad_views av on av.country = ndr.country and av.device_id = ndr.device_id
left join m0_cat_screen mcs on mcs.country = ndr.country and mcs.device_id = ndr.device_id
left join m0_ct_app_launch_ctid calc on ndr.platform_group = 'APP' and calc.clevertapid = ndr.clevertapid_diy
left join m0_ct_app_launch_uid calu on ndr.platform_group = 'APP' and calu.user_id = ndr.user_id
left join m0_ct_vv_ctid cvc on ndr.platform_group = 'APP' and ndr.m0_total_vv > 0 and cvc.clevertapid = ndr.clevertapid_diy
left join m0_ct_vv_uid cvu on ndr.platform_group = 'APP' and ndr.m0_total_vv > 0 and cvu.user_id = ndr.user_id
left join m0_ct_control_group_n_noti_sent_ctid ccg on ndr.platform_group = 'APP' and calc.clevertapid = ccg.clevertapid
left join bi.d_usr_reg dur on dur.usr_cms_id = ndr.user_id
left join mi_dev.group_category_name gcn on ndr.acqusition_cat_name = gcn.cat_name
left join mi_dev.group_category_name gcn2 on ndr.acqusition_analytical_cat_name = gcn2.cat_name
left join mi_dev.sol_acc_map sam on dur.sol_acc_typ = sam.sol_acc_typ
left join all_titles att on	ms.last_search_kw = att.series_name
left join mi_dev.organic_utm2gc_mapping ougm_af
					on nvl(ougm_af.utm_src, '') = aits.af_media_source
					and ougm_af.utm_medium = '$no_utm_medium$'
					and ndr.country = ougm_af.country
left join mi_dev.organic_utm2gc_mapping ougm on
					nvl(ougm.utm_src, '') = nvl(ndr.acquisition_utm_source, '')
					and nvl(ougm.utm_medium, '') = nvl(ndr.acquisition_utm_medium, '')
					and ndr.country = ougm.country
left join mi_dev.referral_main_domain_map rmdm on
					ndr.acquisition_traffic_source = rmdm.traffic_source
					and ndr.acquisition_traffic_domain = rmdm.traffic_domain
					and ndr.country = rmdm.country